package com.example.authentaka

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth

class RegistrationActivity : AppCompatActivity() {

    private lateinit var editTextEmail: TextInputEditText
    private lateinit var editTextPassword: TextInputEditText
    private lateinit var editTextConfirmPassword: TextInputEditText
    private lateinit var buttonRegistration: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        init()
        registerListeners()
    }

    private fun init(){
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        buttonRegistration = findViewById(R.id.buttonRegistration)
        editTextConfirmPassword = findViewById(R.id.editTextConfirmPassword)
    }

    private fun registerListeners(){
        buttonRegistration.setOnClickListener{
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val confirmPassword = editTextConfirmPassword.text.toString()

            if(email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Email or Password is blank", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (confirmPassword != password) {
                Toast.makeText(this, "Confirm password failed.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!password.matches(".*[A-Z].*".toRegex())) {
                Toast.makeText(this, "Password must contain one upper-case character", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!password.matches(".*[a-z].*".toRegex())) {
                Toast.makeText(this, "Password must contain one lower-case character", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!password.matches(".*[0-9].*".toRegex())) {
                Toast.makeText(this, "Password must contain one number", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)

                .addOnCompleteListener { task ->
                    if (task.isSuccessful){
                        startActivity(Intent(this,Loginactivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(this, "Operation Unsuccessful", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
}